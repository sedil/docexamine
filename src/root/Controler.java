package root;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.bson.Document;

import util.DBAccess;
import util.JSONBuilder;
import util.TempData;
import util.TermVector;
import util.ThreadHelper;
import algorithm.Cooccurrence;
import algorithm.CosineSimilarity;
import algorithm.DiceCoefficient;
import algorithm.FastText;
import algorithm.Jaccard;
import algorithm.Jaro;
import algorithm.Pagerank;
import algorithm.TFIDF;
import algorithm.Word2Vec;
import fileprocessing.FileCollector;

/**
 * This is the control class
 * @author sebastian
 */
public class Controler {

	private FileCollector _fc;
	private JSONBuilder _jsonbuilder;
	
	private List<TermVector> _tvlist; // für cosine, jaccard
	private List<List<String>> _data;
	private List<Set<String>> _keywords;
	private List<Map<String, String>> _jsondata;
	
	private final String DB_HOST;
	private final String DB_PASSWORD;
	private final int DB_PORT;
	private final String DB_USER;
	private final String DB_COLLECTION;
	private final String DB_DATABASE;
	private String _jsonfilename;
	private final String JSONPATH;
	private final int NUMBER_OF_KEYWORDS;
	private final int NUMBER_OF_THREADS;
	private final int COOCCURRENCE_WINDOW_SIZE;
	private final int MIN_SIMILARITY_SCORE;
	
	private final String TMP_WORDLIST_PATH;
	private final String TMP_KEYWORD_PATH;
	private final String TMP_RELWORD_PATH;
	
	/**
	 * Constructor
	 * @param config contains all settings for this application
	 */
	public Controler(final List<String> config){

		_keywords = new ArrayList<Set<String>>();
		
		/* get the maximum number of keywords to extract (if available) */
		NUMBER_OF_KEYWORDS = Integer.parseInt(config.get(2));
		NUMBER_OF_THREADS = Integer.parseInt(config.get(3));
		COOCCURRENCE_WINDOW_SIZE = Integer.parseInt(config.get(4));
		
		if ( Integer.parseInt(config.get(5)) < 0 || Integer.parseInt(config.get(5)) > 100){
			MIN_SIMILARITY_SCORE = 0;
		} else {
			MIN_SIMILARITY_SCORE = Integer.parseInt(config.get(5));
		}
		
		JSONPATH = config.get(14);
		TMP_WORDLIST_PATH = config.get(16);
		TMP_KEYWORD_PATH = config.get(17);
		TMP_RELWORD_PATH = config.get(18);
		
		/* get the access data for the database */
		DB_HOST = config.get(8);
		DB_PORT = Integer.parseInt(config.get(9));
		DB_PASSWORD = config.get(10);
		DB_USER = config.get(11);
		DB_COLLECTION = config.get(12);
		DB_DATABASE = config.get(13);
		
		boolean useMongoDB = false;
		System.out.println("Would you use MongoDB to save the results? (y)es, (n)o : ");
		Scanner readInput = new Scanner(System.in);
		String answer = readInput.nextLine();
		if ( answer.equals("y") ){
			useMongoDB = true;
		}
		readInput.close();
		
		/* get the path to the documentpool */
		_fc = new FileCollector(config.get(0), config.get(15), NUMBER_OF_THREADS); // directory, stopwords
		_fc.collectDocumentContent();
		_data = _fc.getDocumentContent();
		_jsondata = _fc.getJsonContent();
		
		if( _data.size() == 0 || _jsondata.size() == 0){
			System.out.println("Load documents ... failed");
			System.out.println("[ERROR] Could not get content from one or more documents. Exit");
			System.exit(0);
			return;
		}
		System.out.println("Load documents ... success");
		
		/* get the algorithm which should weight the keywords */
		String key_algorithm = config.get(1);
		switch(key_algorithm){
			case "tfidf" : {
				tfidf();
				break;
			}
			case "pagerank" : {
				pagerank();
				break;
			}
			default : {
				System.err.println("Unknown algorithm '" + key_algorithm + "' using tfidf");
				tfidf();
			}
		}
		
		System.out.println("extract keywords ... success");

		/* get the algorithm to compare the documents */
		String doc_compare = config.get(6);
		switch(doc_compare){
			case "cosine" : {
				cosine();
				break;
			}
			case "jaccard" : {
				jaccard();
				break;
			}
			default : {
				System.err.println("Unknown algorithm '" + doc_compare + "' using cosine");
				cosine();
			}
		}
		
		System.out.println("compare document similiarity ... success");

		/* get the algorithm to compare two strings */
		String str_compare = config.get(7);
		switch(str_compare){
			case "dice" : {
				dice();
				break;
			}
			case "jaro" : {
				jaro();
				break;
			}
			case "co-occurrence" : {
				cooccurrence(COOCCURRENCE_WINDOW_SIZE);
				break;
			}
			case "word2vec" : {
				word2vec();
				break;
			}
			case "fasttext" : {
				fasttext();
				break;
			}
			default : {
				System.err.println("Unknown algorithm '" + str_compare + "' using dice");
				dice();
			}
		}
		
		System.out.println("find related keywords ... success");
		
		_data.clear();
		_keywords.clear();

		final String jsonString = builtJSON();
		if ( useMongoDB ){
			connectToMongoDB(jsonString);
		} else {
			_jsonbuilder.saveJson();
			System.out.println("save result ... success");
		}
	}
	
	/**
	 * executes the term-frequency-inverted-document-frequency (TF-IDF)
	 * algorithm
	 */
	private void tfidf(){
		
		ExecutorService exec = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
		_tvlist = new ArrayList<TermVector>();
		List<Future<ThreadHelper<TermVector>>> tasks = new ArrayList<Future<ThreadHelper<TermVector>>>();
		for(int i = 0; i < _data.size(); i++){
			_tvlist.add(new TermVector());		
			final Future<ThreadHelper<TermVector>> future = exec.submit(new TFIDF(_data,_data.get(i),i));
			tasks.add(future);
		}
		
		List<ThreadHelper<TermVector>> result = new ArrayList<ThreadHelper<TermVector>>();
		int j = 0;
		for(Future<ThreadHelper<TermVector>> f : tasks ){
			try {
				result.add(f.get(120, TimeUnit.SECONDS));
				j++;
			} catch (InterruptedException | ExecutionException | TimeoutException e) {
				ThreadHelper<TermVector> th = new ThreadHelper<TermVector>(new TermVector(), result.size());
				th.getData().addTerm("!ERROR", 0.0);
				result.add(th);
				e.printStackTrace();
				System.err.println("Timeout for document : " + (j+1));
				j++;
			}
		}
		
		exec.shutdown();
		try {
			exec.awaitTermination(60, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			exec.shutdownNow();
			e.printStackTrace();
		}
		
		// getting the right order
		for(int i = 0; i < _tvlist.size(); i++){				
			_tvlist.get(result.get(i).getIdx()).getTermVector().putAll(result.get(i).getData().getTermVector());			
		}
		
		for(int i = 0; i < _tvlist.size(); i++){
			
			// get the first n keys //
			_tvlist.get(i).sortMap();
			Set<String> keysFromDoc = _tvlist.get(i).getNKeywords(NUMBER_OF_KEYWORDS);
			_keywords.add(keysFromDoc);
			
			StringBuilder sb = new StringBuilder();
			for(String k : keysFromDoc){
				sb.append(k + " ");
			}
			_jsondata.get(i).put("keyword", sb.toString());
		}
	}
	
	/**
	 * executes the pagerank algorihtm
	 */
	private void pagerank(){
		
		ExecutorService exec = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
		_tvlist = new ArrayList<TermVector>();
		List<Future<TempData>> tasks = new ArrayList<Future<TempData>>();
		for(int i = 0; i < _data.size(); i++){
			_tvlist.add(new TermVector());
			_keywords.add(new HashSet<String>());
			final Future<TempData> future = exec.submit(new Pagerank(_data.get(i), i, NUMBER_OF_KEYWORDS, 0.85, 5));
			tasks.add(future);
		}
		
		List<TempData> result = new ArrayList<TempData>();
		int j = 0;
		for(Future<TempData> f : tasks){
			try {
				result.add(f.get(120, TimeUnit.SECONDS));
				j++;
			} catch (InterruptedException | ExecutionException | TimeoutException e) {
				TempData dummy = new TempData(j);
				dummy.getKeywordSet().add("!ERROR");
				dummy.getTermVector().addTerm("!ERROR", 0.0);
				result.add(dummy);
				e.printStackTrace();
				System.err.println("Timeout for document : " + (j+1));
				j++;
			}
		}
		
		exec.shutdown();
		try {
			exec.awaitTermination(60, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			exec.shutdownNow();
			e.printStackTrace();
		}
		
		// getting the right order //
		for(int i = 0; i < _tvlist.size(); i++){
			_tvlist.get(result.get(i).getIndex()).getTermVector().putAll(result.get(i).getTermVector().getTermVector());
			_keywords.get(result.get(i).getIndex()).addAll(result.get(i).getKeywordSet());
		}

		for(int i = 0; i < _tvlist.size(); i++){
			StringBuilder sb = new StringBuilder();
			for(String kw : _keywords.get(i) ){
				sb.append(kw + " ");
			}	
			_jsondata.get(i).put("keyword", sb.toString());	
		}
	}
	
	/**
	 * executes the dice algorithm to compare two words/strings
	 */
	private void dice(){
		/* remove all duplicates from each document */
		List<Set<String>> words = new ArrayList<Set<String>>();
		for(int i = 0; i < _data.size(); i++){
			Set<String> no_dupl = new HashSet<String>(_data.get(i));
			no_dupl.removeAll(_keywords.get(i));
			words.add(no_dupl);
		}
		
		ExecutorService exec = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
		List<Future<String[]>> tasks = new ArrayList<Future<String[]>>();
		for(int i = 0; i < words.size(); i++){
			for(String word : words.get(i)){
				for(Iterator<String> it = _keywords.get(i).iterator(); it.hasNext();){
					String keyword = it.next();
					final Future<String[]> future = exec.submit(new DiceCoefficient(word, keyword, i));
					tasks.add(future);
				}
			}
		}
		
		List<String[]> result = new ArrayList<String[]>();
		System.out.println("compare keywords for related keys ...");
		for(Future<String[]> f : tasks){
			try {
				result.add(f.get(120, TimeUnit.SECONDS));
			} catch (InterruptedException | ExecutionException | TimeoutException e) {
				result.add(new String[]{"" + result.size(),"!ERROR","!ERROR",""});
				e.printStackTrace();
				System.err.println("Timeout for dice");
			}
		}
		
		exec.shutdown();
		try {
			exec.awaitTermination(60, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			exec.shutdownNow();
			e.printStackTrace();
		}
		
		// das jeweilige keyword überspringen
		for(int i = 0; i < _keywords.size(); i++){
			List<String> relatedlist = new ArrayList<String>();
			Map<String,Double> mostRelated = new HashMap<String,Double>();
			int c = NUMBER_OF_KEYWORDS;
			String maxRel = null;
			double max = 0.0;
			for(int j = 0; j < _keywords.get(i).size(); j++){

				for(int k = 0; k < result.size(); k++){
					int docID = Integer.parseInt(result.get(k)[0]); // document id
					if ( docID == i){
						double sim = Double.parseDouble(result.get(k)[3]); // similarity
						if ( sim >= max && sim > 0.0 ){
							max = sim;
							maxRel = result.get(k)[1]; // related word
							mostRelated.put(maxRel, max);
						}
					}
				}
			}
		
			for(Map.Entry<String, Double> elem : mostRelated.entrySet()){
				relatedlist.add(elem.getKey());
				c--;
				if( c == 0){
					break;
				}
			}

			StringBuilder sb = new StringBuilder();
			for(int l = 0; l < relatedlist.size(); l++ ){
				sb.append(relatedlist.get(relatedlist.size() - 1) + " ");
				relatedlist.remove(relatedlist.size() - 1);
			}
			
			_jsondata.get(i).put("related", sb.toString());
		}
	}
	
	/**
	 * executes the jaro algorithm to compate two words
	 */
	private void jaro(){
		/* remove all duplicates from each document */
		List<Set<String>> words = new ArrayList<Set<String>>();
		for(int i = 0; i < _data.size(); i++){
			Set<String> no_dupl = new HashSet<String>(_data.get(i));
			no_dupl.removeAll(_keywords.get(i));
			words.add(no_dupl);
		}
		
		
		ExecutorService exec = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
		List<Future<String[]>> tasks = new ArrayList<Future<String[]>>();
		for(int i = 0; i < words.size(); i++){
			for(String word : words.get(i)){
				for(Iterator<String> it = _keywords.get(i).iterator(); it.hasNext();){
					String keyword = it.next();
					final Future<String[]> future = exec.submit(new Jaro(word, keyword, i));
					tasks.add(future);
				}
			}
		}
		
		List<String[]> result = new ArrayList<String[]>();
		System.out.println("compare keywords for related keys ...");
		for(Future<String[]> f : tasks){
			try {
				result.add(f.get(120, TimeUnit.SECONDS));
			} catch (InterruptedException | ExecutionException | TimeoutException e) {
				result.add(new String[]{"" + result.size(),"!ERROR","!ERROR",""});
				e.printStackTrace();
				System.err.println("Timeout for dice");
			}
		}
		
		exec.shutdown();
		try {
			exec.awaitTermination(60, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			exec.shutdownNow();
			e.printStackTrace();
		}
		
		for(int i = 0; i < _keywords.size(); i++){
			List<String> relatedlist = new ArrayList<String>();
			Map<String,Double> mostRelated = new HashMap<String,Double>();
			int c = NUMBER_OF_KEYWORDS;
			String maxRel = null;
			double max = 0.0;
			for(int j = 0; j < _keywords.get(i).size(); j++){

				for(int k = 0; k < result.size(); k++){
					int docID = Integer.parseInt(result.get(k)[0]); // document id
					if ( docID == i){
						double sim = Double.parseDouble(result.get(k)[3]); // similarity
						if ( sim >= max && sim > 0.0 ){
							max = sim;
							maxRel = result.get(k)[1]; // related word
							mostRelated.put(maxRel, max);
						}
					}
				}
			}
		
			for(Map.Entry<String, Double> elem : mostRelated.entrySet()){
				relatedlist.add(elem.getKey());
				c--;
				if( c == 0){
					break;
				}
			}

			StringBuilder sb = new StringBuilder();
			for(int l = 0; l < relatedlist.size(); l++ ){
				sb.append(relatedlist.get(relatedlist.size()-1) + " ");
				relatedlist.remove(relatedlist.size() - 1);
			}
			
			_jsondata.get(i).put("related", sb.toString());
		}
	}
	
	/**
	 * executes the coocurrence procedure
	 */
	private void cooccurrence(int windowsize){
		
		ExecutorService exec = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
		
		// for the right order //
		List<Set<String>> rightOrder = new ArrayList<Set<String>>();
		
		List<Future<ThreadHelper<Set<String>>>> tasks = new ArrayList<Future<ThreadHelper<Set<String>>>>();
		for(int i = 0; i < _data.size(); i++){
			rightOrder.add(new HashSet<String>());
			final Future<ThreadHelper<Set<String>>> future = exec.submit(new Cooccurrence(_data.get(i),_keywords.get(i),windowsize,i));
			tasks.add(future);
		}
		
		List<ThreadHelper<Set<String>>> result = new ArrayList<ThreadHelper<Set<String>>>();
		System.out.println("run co-occurrence algorithm ...");
		for(Future<ThreadHelper<Set<String>>> f : tasks ){
			try {
				result.add(f.get(120, TimeUnit.SECONDS));
			} catch (InterruptedException | ExecutionException | TimeoutException e) {
				result.add(new ThreadHelper<Set<String>>(new HashSet<String>(),result.size()));
				e.printStackTrace();
			}
		}
		
		exec.shutdown();
		try {
			exec.awaitTermination(60, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			exec.shutdownNow();
			e.printStackTrace();
		}
		
		// getting the right order //
		for(int i = 0; i < result.size(); i++){
			rightOrder.get(result.get(i).getIdx()).addAll(result.get(i).getData());
		}

		for(int i = 0; i < _data.size(); i++){
			
			int counter = NUMBER_OF_KEYWORDS;
			Set<String> related = rightOrder.get(i);
			
			StringBuilder sb = new StringBuilder();
			for(String rel : related){
				sb.append(rel + " ");
				counter--;
				
				if ( counter == 0){
					break;
				}
			}
			
			_jsondata.get(i).put("related", sb.toString());
		}
	}
	
	/**
	 * executes the cosine similarity to compare to sets
	 */
	private void cosine(){
		
		ExecutorService exec = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
		List<Future<Double[]>> tasks = new ArrayList<Future<Double[]>>();
		for(int i = 0; i < _tvlist.size(); i++){
			for(int j = 0; j < _tvlist.size(); j++){
				if ( i == j){
					continue;
				} else {
					final Future<Double[]> future = exec.submit(new CosineSimilarity(_tvlist.get(i).getTermVector(), _tvlist.get(j).getTermVector(),i,j));
					tasks.add(future);
				}
			}
		}
		
		List<Double[]> result = new ArrayList<Double[]>();
		System.out.println("compared documents ...");
		for(Future<Double[]> f : tasks ){
			try {
				result.add(f.get(120, TimeUnit.SECONDS));
			} catch (InterruptedException | ExecutionException | TimeoutException e) {
				result.add(new Double[]{0.0,0.0,0.0});
				e.printStackTrace();
				System.err.println("error by comparing two documents");
			}
		}
		
		exec.shutdown();
		try {
			exec.awaitTermination(60, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			exec.shutdownNow();
			e.printStackTrace();
		}
		
		Map<Integer, Map<Integer, Double>> outer = new HashMap<Integer,Map<Integer, Double>>();
		for(int i = 0; i < result.size(); i++){
			Double[] values = result.get(i);
			
			String[] idx = Double.toString(values[0]).split("\\.");
			Integer docI = Integer.parseInt(idx[0]);
			idx = Double.toString(values[1]).split("\\.");
			Integer docJ = Integer.parseInt(idx[0]);
			
			if ( outer.containsKey(docI) ){
				outer.get(docI).put(docJ, values[2]);
			} else {
				Map<Integer, Double> inner = new HashMap<Integer, Double>();
				inner.put(docJ, values[2]);
				outer.put(docI, inner);
			}
		}

		for(Map.Entry<Integer, Map<Integer, Double>> elem : outer.entrySet()){
			StringBuilder sb = new StringBuilder();
			Integer outerkey = elem.getKey(); // Document i
			for(Map.Entry<Integer, Double> ref : elem.getValue().entrySet()){
				
				Integer innerkey = ref.getKey(); // Document j
				Double sim = ref.getValue(); // similarity between i and j

				if ( 100 * sim >= MIN_SIMILARITY_SCORE ){
					sb.append(_jsondata.get(innerkey).get("file") + " ");
				}
			}
			
			if ( sb.length() > 0 ){
				_jsondata.get(outerkey).put("related_doc", sb.toString());
			}
		}
	}
	
	/**
	 * executes the jaccard-weight algorithm to compare two sets
	 */
	private void jaccard(){
		
		ExecutorService exec = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
		List<Future<Double[]>> tasks = new ArrayList<Future<Double[]>>();
		for(int i = 0; i < _tvlist.size(); i++){
			for(int j = 0; j < _tvlist.size(); j++){
				if ( i == j){
					continue;
				} else {
					final Future<Double[]> future = exec.submit(new Jaccard(_tvlist.get(i).getTermVector(), _tvlist.get(j).getTermVector(),i,j));
					tasks.add(future);
				}
			}
		}
		
		List<Double[]> result = new ArrayList<Double[]>();
		System.out.println("compared documents ...");
		for(Future<Double[]> f : tasks ){
			try {
				result.add(f.get(120, TimeUnit.SECONDS));
			} catch (InterruptedException | ExecutionException | TimeoutException e) {
				result.add(new Double[]{0.0,0.0,0.0});
				e.printStackTrace();
				System.err.println("error by comparing two documents");
			}
		}
		
		exec.shutdown();
		try {
			exec.awaitTermination(60, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			exec.shutdownNow();
			e.printStackTrace();
		}
		
		Map<Integer, Map<Integer, Double>> outer = new HashMap<Integer,Map<Integer, Double>>();
		for(int i = 0; i < result.size(); i++){
			Double[] values = result.get(i);
			
			String[] idx = Double.toString(values[0]).split("\\.");
			Integer docI = Integer.parseInt(idx[0]);
			idx = Double.toString(values[1]).split("\\.");
			Integer docJ = Integer.parseInt(idx[0]);
			
			if ( outer.containsKey(docI) ){
				outer.get(docI).put(docJ, values[2]);
			} else {
				Map<Integer, Double> inner = new HashMap<Integer, Double>();
				inner.put(docJ, values[2]);
				outer.put(docI, inner);
			}
		}

		for(Map.Entry<Integer, Map<Integer, Double>> elem : outer.entrySet()){
			StringBuilder sb = new StringBuilder();
			Integer outerkey = elem.getKey(); // Document i
			for(Map.Entry<Integer, Double> ref : elem.getValue().entrySet()){
				
				Integer innerkey = ref.getKey(); // Document j
				Double sim = ref.getValue(); // similarity between i and j
				
				if ( 100 * sim >= MIN_SIMILARITY_SCORE  ){
					sb.append(_jsondata.get(innerkey).get("file") + " ");
				}
			}
			
			if ( sb.length() > 0 ){
				_jsondata.get(outerkey).put("related_doc", sb.toString());
			}
			
		}
	}
	
	/**
	 * executes the word2vec
	 */
	private void word2vec(){
		/* collect all words into one single set */
		Set<String> wordlist = new HashSet<String>();
		for(List<String> doc : _data){
			wordlist.addAll(doc);
		}

		Word2Vec w2v = new Word2Vec(wordlist, _keywords, TMP_WORDLIST_PATH, TMP_KEYWORD_PATH, TMP_RELWORD_PATH);
		w2v.saveWordlist();
		w2v.saveKeywords();
		System.out.println("start word2vec python script ...");
		w2v.start();
		List<Set<String>> related = w2v.relwordsToDocument();
		
		for(int i = 0; i < related.size(); i++){
			Set<String> relwords = related.get(i);
			int maxRels = NUMBER_OF_KEYWORDS;
			StringBuilder sb = new StringBuilder();
			for(String relPerDoc : relwords ){
				if ( maxRels == 0){
					break;
				} else {
					sb.append(relPerDoc+" ");
					maxRels--;
				}
			}
			_jsondata.get(i).put("related", sb.toString());
		}
	}
	
	/**
	 * executes the fasttext
	 */
	private void fasttext(){
		/* collect all words into one single set */
		Set<String> wordlist = new HashSet<String>();
		for(List<String> doc : _data){
			wordlist.addAll(doc);
		}

		FastText ft = new FastText(wordlist, _keywords, TMP_WORDLIST_PATH, TMP_KEYWORD_PATH, TMP_RELWORD_PATH);
		ft.saveWordlist();
		ft.saveKeywords();
		System.out.println("start fasttext python script ...");
		ft.start();
		List<Set<String>> related = ft.relwordsToDocument();
		
		for(int i = 0; i < related.size(); i++){
			Set<String> relwords = related.get(i);
			int maxRels = NUMBER_OF_KEYWORDS;
			StringBuilder sb = new StringBuilder();
			for(String relPerDoc : relwords ){
				if ( maxRels == 0){
					break;
				} else {
					sb.append(relPerDoc+" ");
					maxRels--;
				}
			}
			_jsondata.get(i).put("related", sb.toString());
		}
	}
	
	/**
	 * builts the json file
	 * @return the json file as string
	 */
	private String builtJSON(){
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		_jsonfilename = timeStamp + ".json";
		_jsonbuilder = new JSONBuilder(JSONPATH,_jsonfilename);
		
		for(int i = 0; i < _jsondata.size(); i++){

			_jsonbuilder.newDocument();
			for(Map.Entry<String, String> elem : _jsondata.get(i).entrySet() ){
				
				if ( elem.getKey().equals("keyword") ||
					elem.getKey().equals("related") ||
					elem.getKey().equals("related_doc")) {
					String[] values = elem.getValue().split(" ");
					_jsonbuilder.insertAttribute(elem.getKey(), values, i);
				} else {
					_jsonbuilder.insertAttribute(elem.getKey(), elem.getValue(), i);
				}
			}
		}
		_jsonbuilder.wrapDocuments();
		return _jsonbuilder.getJSONString();
	}
	
	/**
	 * tries to insert the jsonfile into the MongoDB
	 * @param json jsonfile
	 */
	private void connectToMongoDB(final String json){
		StringBuilder sb = new StringBuilder();
		sb.append("{'jsondata':");
		sb.append(json);
		sb.append("}");
		Document doc = Document.parse(sb.toString());
		
		DBAccess mongo = new DBAccess(DB_HOST, DB_PORT);
		mongo.setUserdata(DB_USER, DB_PASSWORD);
		mongo.setMongoDBData(DB_DATABASE, DB_COLLECTION);
		mongo.connectToDatabase();
		boolean inserted = mongo.insertDocument(doc);
		
		if ( !inserted ){
			_jsonbuilder.saveJson();
		}
		mongo.close();
	}
	
	/**
	 * returns all words per document
	 * @return a list which contains a sublist with
	 * all words per document
	 */
	public List<List<String>> getData(){
		return _data;
	}
	
	/**
	 * returns the jsondata per document
	 * @return jsondata
	 */
	public List<Map<String, String>> getJson(){
		return _jsondata;
	}
	
	/**
	 * returns the termvector
	 * @return termvectorlist
	 */
	public List<TermVector> getTermVector(){
		return _tvlist;
	}
	
	/**
	 * returns all keywords from each document
	 * @return keywordlist
	 */
	public List<Set<String>> getKeywords(){
		return _keywords;
	}
}
