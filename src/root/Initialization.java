package root;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This is the first class of this application. It reads the configuration
 * file and initilizes parameters to work
 * @author sebastian
 */
public class Initialization {
	
	private List<String> _initvalues;
	private static boolean _missingConfigFile;
	
	/**
	 * Constructor reads in the configuration file
	 * @param configfile configuration file
	 */
	public Initialization(final String configfile) {
		File file = new File(configfile);
		_missingConfigFile = true;
		
		try {
			_initvalues = getInitialContent(file);
			_missingConfigFile = false;
		} catch ( Exception e ){
			e.printStackTrace();
			try {
				file = new File(System.getProperty("user.dir") + "/init.conf");
				FileWriter fw = new FileWriter(file);
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write("# Configurationfile was missing! Generated default configurations" + "\n");
				bw.write("# path to the documentpool. Allowed are *.txt, *.html and *.pdf" + "\n");
				bw.write("documentpool=" + System.getProperty("user.dir") + "/sample/" + "\n");
				bw.write("# algorithm to extract keywords. Supported are : tfidf, pagerank" + "\n");
				bw.write("keyword=tfidf" + "\n");
				bw.write("# amount of keywords to extract if available" + "\n");
				bw.write("keynumber=5" + "\n");
				bw.write("# number of threads" + "\n");
				bw.write("threads=2" + "\n");
				bw.write("# size of the window for building co-occurrences" + "\n");
				bw.write("cooc-size=2" + "\n");
				bw.write("# minimum similarity score to list related documents [0;100]" + "\n");
				bw.write("sim-score=5" + "\n");
				bw.write("# algorithm to compare documents. Supported are : cosine, jaccard" + "\n");
				bw.write("doccompare=cosine" + "\n");
				bw.write("# algorithm to compare words. Supported are : dice, jaro, word2vec, fasttext, co-occurrence" + "\n");
				bw.write("wordcompare=dice" + "\n");
				bw.write("# accessdata for MongoDB" + "\n");
				bw.write("host=127.0.0.1" + "\n");
				bw.write("port=27017" + "\n");
				bw.write("password=root" + "\n");
				bw.write("user=sampleUser" + "\n");
				bw.write("collection=sampleCollection" + "\n");
				bw.write("database=sampleDB" + "\n");
				bw.write("jsonPath=" + System.getProperty("user.dir") + "/result/" + "\n");
				bw.write("stopwords=" + System.getProperty("user.dir") + "/stopwords.txt" + "\n");
				bw.write("# temp path for word2vec" + "\n");
				bw.write("pythonWordlist=" + System.getProperty("user.dir") + "/wordlist.txt" + "\n");
				bw.write("pythonKeywords=" + System.getProperty("user.dir") + "/keywords.txt" + "\n");
				bw.write("pythonRelwords=" + System.getProperty("user.dir") + "/relwords.txt" + "\n");
				bw.flush();
				bw.close();
				fw.close();
				_initvalues = getInitialContent(file);
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}
	
	/**
	 * reads the content of the configuration file
	 * @param file config
	 * @return list of the content
	 * @throws IOException issue with the file
	 */
	private List<String> getInitialContent(final File file) throws IOException {
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		List<String> initvalues = new ArrayList<String>();
			
		boolean read = true;
		while( read ) {
			String line = br.readLine();
				
			if ( line == null ){
				read = false;
				break;
			}
				
			if ( line.isEmpty() ){
				continue;
			}

			if ( !line.startsWith("#") ){
				String[] pair = line.split("=");
				if ( pair.length < 2 ){
					read = false;
					System.err.println("One of the lines in the config file are invalid");
					break;
				} else {
					initvalues.add(pair[1]);
				}
			}
		}
		br.close();
		fr.close();
		return initvalues;
	}
	
	/**
	 * returns the list with config parameters
	 * @return settings
	 */
	public List<String> getConfiguration(){
		return _initvalues;
	}
	
	/**
	 * main
	 * @param args arguments
	 */
	public static void main(String[] args){
		
		if ( args.length == 1){
			Initialization init = new Initialization(args[0]);
			
			if ( _missingConfigFile ){
				System.out.println("Missing configuration file. Generated a default config file");
				return;
			}
			
			if ( init.getConfiguration().size() != 19 ){
				System.err.println("Some lines are missing");
			} else {
				try {
					Integer.parseInt(init.getConfiguration().get(2)); //amount of keywords
					Integer.parseInt(init.getConfiguration().get(3)); //amount of threads
					Integer.parseInt(init.getConfiguration().get(4)); //co-occurrence windowsize
					Integer.parseInt(init.getConfiguration().get(5)); //minimum similarityscore
					Integer.parseInt(init.getConfiguration().get(9)); //MongoDB portnumber
					System.out.println("Load config file ... success");
					new Controler(init.getConfiguration());
				} catch ( NumberFormatException nfe ){
					nfe.printStackTrace();
					System.out.println("Load config file ... failed");
					System.err.println("[ERROR] Number of Keys / Number of Threads or Portnumber are not numbers");
				}
			}
			
		} else {
			System.out.println("missing parameter for the location of the configurationfile. If this is the first start then type as parameter 'x' to generate a default configuration file");
		}
	}

}
