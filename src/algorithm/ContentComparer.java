package algorithm;

/**
 * A interface for the classes which compares the
 * similarities from documents
 * @author sebastian
 *
 * @param <T> generic parameter
 */
public interface ContentComparer<T> {
	
	/**
	 * compares the similarity between the to objects
	 * x and y
	 * @param setX object x
	 * @param setY object y
	 * @return similarity value as double between 0 and 1
	 */
	public double similarity(final T setX, final T setY);

}
