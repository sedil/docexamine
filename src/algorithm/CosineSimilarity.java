package algorithm;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 * This class implements the cosine similarity to compare two sets.
 * Each Set is a vector of weighted terms to calculate the document
 * similarity
 * @author sebastian
 *
 */
public class CosineSimilarity implements ContentComparer<Map<String, Double>>, Callable<Double[]>{
	
	private Map<String, Double> _x;
	private Map<String, Double> _y;
	private int _docI;
	private int _docJ;
	
	/**
	 * Default Constructor
	 */
	public CosineSimilarity() { }
	
	/**
	 * Default Constructor for ExecutorService
	 * @param x set one
	 * @param y set two
	 * @param i document with index i
	 * @param j document with index j
	 */
	public CosineSimilarity(Map<String, Double> x, Map<String, Double> y, int i, int j) { 
		_x = x;
		_y = y;
		_docI = i;
		_docJ = j;
	}

	@Override
	public double similarity(final Map<String, Double> vecA, final Map<String, Double> vecB){
		if ( vecA == null || vecB == null ){
			System.err.println("one or both vectors are null");
			return 0.0;
		}

		/* compute the intersection of the two wordvectors */
		Set<String> intersection = new HashSet<String>(vecA.keySet());
		intersection.retainAll(vecB.keySet());
		
		if ( intersection.isEmpty() ){
			return 0.0;
		}		

		/* calculate the dotproduct of the words in the intersection set */
		double dot = 0.0;
		for(String word : intersection){
			double temp = vecA.get(word) * vecB.get(word);
			dot += temp;
		}
		
		double distA = 0.0;
		double distB = 0.0;
		for(double weight : vecA.values()){
			distA += Math.pow(weight, 2);
		}
		
		for(double weight : vecB.values()){
			distB += Math.pow(weight, 2);
		}
		
		double similarity = 0.0;
		if ( distA <= 0.0 || distB <= 0.0){
			return similarity;
		} else {
			similarity = (dot)/(Math.sqrt(distA) * Math.sqrt(distB));
		}
		return similarity;
	}

	@Override
	public Double[] call() throws Exception {
		Double[] values = new Double[3];
		values[0] = (double) _docI;
		values[1] = (double) _docJ;
		values[2] = similarity(_x,_y);
		return values;
	}

}
