package algorithm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

import util.ThreadHelper;
import util.WordPair;

/**
 * This class builts word cooccurrences from a given list of strings/words.
 * Supported are the direct neighbour methode, window methode and complete methode
 * @author sebastian
 */
public class Cooccurrence implements Callable<ThreadHelper<Set<String>>>{
	
	private final int WINDOW_SIZE;
	private final int IDX;
	
	private List<String> _wordsFromDoc;
	private Map<WordPair, Integer> _wordpairmap;
	private Set<String> _keywords;
	private Set<String> _relatedKeys;

	/**
	 * Constructor which initializes a wordpairlist.
	 * Called from Pagerank and Hits
	 * @param document list of words from a document
	 */
	public Cooccurrence(final List<String> document){
		WINDOW_SIZE = 5;
		IDX = -1;
		_wordsFromDoc = document;
		_wordpairmap = new HashMap<WordPair, Integer>();
		_relatedKeys = new HashSet<String>();	
	}
	
	/**
	 * Constructor which initializes a wordpairlist for ExecutorService.
	 * @param document list of words from a document
	 * @param keywords list of keywords to find related keys
	 * @param windowsize size of the window to built co-occurrences
	 * @param idx document id
	 */
	public Cooccurrence(final List<String> document, Set<String> keywords, int windowsize, int idx){
		WINDOW_SIZE = windowsize;
		IDX = idx;
		_wordsFromDoc = document;
		_keywords = keywords;
		_wordpairmap = new HashMap<WordPair, Integer>();
		_relatedKeys = new HashSet<String>();
	}

	/**
	 * builts all possible word cooccurrences without duplicates
	 */
	public void builtCooccurrencesComplete(){
		for(int i = 0; i < _wordsFromDoc.size(); i++){
			for(int j = i + 1; j < _wordsFromDoc.size(); j++){
				String x = _wordsFromDoc.get(i);
				String y = _wordsFromDoc.get(j);
				
				if ( x.isEmpty() || y.isEmpty() ){
					continue;
				}
				
				if( !x.equalsIgnoreCase(y) ){
					
					WordPair pair = new WordPair(x,y);
					if( _wordpairmap.containsKey(pair) ){
						int amount = _wordpairmap.get(pair);
						_wordpairmap.put(pair, amount + 1);
					} else {
						_wordpairmap.put(new WordPair(x,y), 1);
					}
				}
			}
		}
		removeEntries();
	}

	/**
	 * builts all possible word cooccurrences but with a windowsize of 2
	 * Example : (n-2,n-1,n,n+1,n+2)
	 */
	public void builtCooccurrencesWindow(){
		
		/* If one document has fewer words than this window size */
		if ( _wordsFromDoc.size() <= 2 * WINDOW_SIZE + 1){
			builtCooccurrencesComplete();
			return;
		}

		/* inital run */
		for(int i = 0; i < 2 * WINDOW_SIZE + 1; i++){
			for(int j = i + 1; j < 2 + WINDOW_SIZE + 1; j++){
				String x = _wordsFromDoc.get(i);
				String y = _wordsFromDoc.get(j);
				
				if( !x.equalsIgnoreCase(y) ){
					
					WordPair pair = new WordPair(x,y);
					if( _wordpairmap.containsKey(pair) ){
						int amount = _wordpairmap.get(pair);
						_wordpairmap.put(pair, amount + 1);
					} else {
						_wordpairmap.put(new WordPair(x,y), 1);
					}
				}
			}
		}
		
		int startidx = 1;
		int endidx = 2 * WINDOW_SIZE + 1;
		
		/* continious run */
		for(int c = 0; c < _wordsFromDoc.size() - 2 * WINDOW_SIZE - 1; c++){
			for(int i = startidx; i < endidx; i++){
				String x = _wordsFromDoc.get(i);
				String y = _wordsFromDoc.get(endidx);
				
				if( !x.equalsIgnoreCase(y) ){
					
					WordPair pair = new WordPair(x,y);
					if( _wordpairmap.containsKey(pair) ){
						int amount = _wordpairmap.get(pair);
						_wordpairmap.put(pair, amount + 1);
					} else {
						_wordpairmap.put(new WordPair(x,y), 1);
					}
				}
			}
			startidx++;
			endidx++;
		}
		removeEntries();
	}
	
	/**
	 * searchs for related keys with the direct neighbour methode
	 */
	private void searchRelatedKeysFromKey(){
		
		for(String keyword : _keywords){

			for(Map.Entry<WordPair, Integer> elem : _wordpairmap.entrySet()){
				WordPair wp = elem.getKey();
	
				if( wp.getLeftElement().equals(keyword) ){
					_relatedKeys.add(wp.getRightElement());
				} else if ( wp.getRightElement().equals(keyword) ){
					_relatedKeys.add(wp.getLeftElement());
				}
			}
		}
	}
	
	/**
	 * builts all possible word cooccurrences of direct neighbours
	 */
	public void builtCooccurrencesNeighbour(){
		for(int i = 1; i < _wordsFromDoc.size() - 1; i++){
			String x = _wordsFromDoc.get(i-1);
			String y = _wordsFromDoc.get(i);
			
			if( !x.equalsIgnoreCase(y) ){
				
				WordPair pair = new WordPair(x,y);
				if( _wordpairmap.containsKey(pair) ){
					int amount = _wordpairmap.get(pair);
					_wordpairmap.put(pair, amount + 1);
				} else {
					_wordpairmap.put(new WordPair(x,y), 1);
				}
			}
		}
		removeEntries();
	}
	
	/**
	 * calculates the amount of cooccurrences and removes all
	 * unimportant entries from the map to reduce the graph
	 */
	private void removeEntries(){
		
		// exermine how often a wordpair occurs together
		int total = 0;
		for(Map.Entry<WordPair, Integer> entry : _wordpairmap.entrySet()){
			total += entry.getValue();
		}

		// take only cooccurrences which appears more then min
		int min = (int) Math.ceil((double) total / (double) _wordpairmap.size());
		Map<WordPair, Integer> temp = new HashMap<WordPair, Integer>();				
		for(Map.Entry<WordPair, Integer> entry : _wordpairmap.entrySet()){
			if ( entry.getValue() >= min ){
				temp.put(entry.getKey(), entry.getValue());
			}
		}
		_wordpairmap = temp;
	}
	
	/**
	 * returns the wordpairs
	 * @return wordpairs as a map of cooccurrences
	 */
	public Map<WordPair, Integer> getWordPairs(){
		return _wordpairmap;
	}
	
	/**
	 * returns related keys.
	 * @return list of strings if availabe
	 */
	public Set<String> getRelatedKeys(){
		return _relatedKeys;
	}

	@Override
	public ThreadHelper<Set<String>> call() throws Exception {
		
		builtCooccurrencesWindow();
		searchRelatedKeysFromKey();
		ThreadHelper<Set<String>> helper = new ThreadHelper<Set<String>>(getRelatedKeys(), IDX);
		return helper;
	}
}
