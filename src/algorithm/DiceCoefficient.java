package algorithm;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 * Implements the dice coefficient. This algorithm compares two
 * strings.
 * @author sebastian
 *
 */
public class DiceCoefficient implements ContentComparer<String>, Callable<String[]> {
	
	private String _x;
	private String _y;
	private int _docID;
	
	/**
	 * Default Constructor
	 */
	public DiceCoefficient() {}
	
	/**
	 * Default Constructor for ExecutorService
	 * @param x word
	 * @param y keyword
	 * @param docID document id
	 */
	public DiceCoefficient(String x, String y, int docID) {
		_x = x;
		_y = y;
		_docID = docID;
	}
	
	@Override
	public double similarity(final String x, final String y){
		
		if ( x == null || y == null ){
			return 0.0;
		}
		
		if ( x.isEmpty() || y.isEmpty() ){
			return 0.0;
		}
		
		/* Both words are equal */
		if( x.equals(y) ){
			return 1.0;
		}
		
		/* Build a bigram of the two string inputs */
		Set<String> bigram_x = createBigram(x);
		Set<String> bigram_y = createBigram(y);
		
		/* get the length of the sets */
		int set_x = bigram_x.size();
		int set_y = bigram_y.size();
		
		/* create the intersection of the bigrams X and Y */
		bigram_x.retainAll(bigram_y);
		int elems = bigram_x.size();
		
		double value = (2 * elems )/(double)(set_x + set_y);
		return value;
	}
	
	/**
	 * creates a bigram of the given word
	 * @param word word
	 * @return a set which contains the bigram of the word
	 */
	private Set<String> createBigram(final String word){
		List<String> bigram = new ArrayList<String>();
		
		if ( word.length() < 2){
			bigram.add(word);
			return new HashSet<String>(bigram);
		}
		
		/* Build the bigram of the input word */
		for(short i = 1; i < word.length(); i++){
			String temp = new String();
			temp += word.charAt(i-1);
			temp += word.charAt(i);
			bigram.add(temp);
		}
		return new HashSet<String>(bigram);
	}

	@Override
	public String[] call() throws Exception {
		String[] values = new String[4];
		values[0] = new String(""+_docID);
		values[1] = _x;
		values[2] = _y;
		values[3] = Double.toString(similarity(_x, _y));
		return values;
	}

}
