package fileprocessing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.DatatypeConverter;


/**
 * This class extract all words from a simple plaintext document
 * @author sebastian
 *
 */
public class PlainExtractor implements TextExtractor {
	
	private final File _file;
	private String _stopwordpath;
	private Map<String, String> _jsondata;
	private List<String> _content;
	
	private String _filename;
	
	/**
	 * Default Constructor
	 * @param file file
	 */
	public PlainExtractor(File file) {
		_file = file;
		_jsondata = new HashMap<String, String>();
		_content = new ArrayList<String>();
	}
	
	@Override
	public void setStopwordFilepath(String stopwordfile){
		_stopwordpath = stopwordfile;
	}
	
	@Override
	public boolean extractText(){
		_filename = _file.getName();
		FileReader fr = null;
		BufferedReader br = null;
		try {
			fr = new FileReader(_file);
			br = new BufferedReader(fr);
			
			boolean read = true;
			List<String> textfieldlist = new ArrayList<String>();
			short max = 255;
			while( read ){
				String line = br.readLine();
				
				if ( line == null ){
					read = false;
					break;
				}
				
				if ( line.isEmpty() ){
					continue;
				}
				
				String[] words = line.replaceAll("\\p{P}|\\d", "").toLowerCase().split("\\s+");
				
				/* allow only characters between a and z */ 
				for(String word : words ){
			        word = word.replace("ä", "ae");
			        word = word.replace("ö", "oe");
			        word = word.replace("ü", "ue");
			        word = word.replace("ß", "ss");
					boolean undefChar = false;
					for(int i = 0; i < word.length(); i++){
						char c = word.charAt(i);
						
						if( c < 97 || c > 122 ){
							undefChar = true;
							break;
						}
					}
					
					if ( !undefChar){
						if ( max > 0 ){
							textfieldlist.add(word.replaceAll("\\R"," "));
							max--;
						}
						if ( word.length() >= 4){
							_content.add(word.replaceAll("\\R"," "));
						}
					}
				}
			}
			
			if ( _content.isEmpty() ){
				br.close();
				fr.close();
				return false;
			}
			
			StringBuilder text = new StringBuilder();
			for(String word : textfieldlist){
				text.append(word+" ");
			}
			
			_jsondata.put("text", text.toString());
			final String hash = hash(text.toString()); 
			_jsondata.put("hash", hash);
			_jsondata.put("file", _filename.replaceAll("\\s+", ""));
			
			br.close();
			fr.close();
			return true;
		} catch ( IOException ioe){
			System.err.println("could not extract file content from : '"+_file.getAbsolutePath() + "' (invalid path)");
			return false;
		}
	}
	
	@Override
	public void removeStopwords() throws FileNotFoundException {
		Set<String> stopwords = new HashSet<String>();
		File file = new File(_stopwordpath);
		
		try {
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			
			boolean read = true;
			while( read ){
				String line = br.readLine();
				if ( line == null ){
					read = false;
					break;
				}
				
				if ( line.isEmpty() ){
					continue;
				}
				
				stopwords.add(line);

			}
			_content.removeAll(stopwords);
			br.close();
			fr.close();
		} catch ( IOException ioe){
			System.err.println("could not remove stopwords. '" + _stopwordpath + "' not found");
		}
	}
	
	@Override
	public String hash(final String text){
		try {
			byte[] textbytes = text.getBytes("UTF-8");
			MessageDigest sha = MessageDigest.getInstance("SHA-512");
			sha.update(textbytes);
			byte[] shabytes = sha.digest();
			String hashwert = DatatypeConverter.printHexBinary(shabytes);
			return hashwert.toLowerCase();
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			//e.printStackTrace();
			return String.valueOf(text.hashCode());
		}
	}
	
	@Override
	public Map<String, String> getJsonContent(){
		return _jsondata;
	}
	
	@Override
	public List<String> getDocumentContent(){
		return _content;
	}

	@Override
	public TextExtractor call() throws Exception {
		if ( !extractText() ){
			return this;
		}
		
		try {
			removeStopwords();
		} catch ( FileNotFoundException fnfe ){
			//fnfe.printStackTrace();
		}
		
		return this;
	}
}
