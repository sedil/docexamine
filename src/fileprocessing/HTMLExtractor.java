package fileprocessing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.DatatypeConverter;

import org.jsoup.Jsoup;


/**
 * This class extracts a text of a html file.
 * Requires 'org.jsoup' to work
 * @author sebastian
 *
 */
public class HTMLExtractor implements TextExtractor {
	
	private final File _file;
	private String _stopwordpath;
	private Map<String, String> _jsondata;
	private List<String> _content;
	
	/**
	 * Default Constructor
	 * @param file html-file to extract
	 */
	public HTMLExtractor(final File file) {
		_file = file;
		_jsondata = new HashMap<String, String>();
		_content = new ArrayList<String>();
	}
	
	@Override
	public void setStopwordFilepath(String stopwordfile){
		_stopwordpath = stopwordfile;
	}
	
	@Override
	public boolean extractText(){
		try {
			String content = Jsoup.parse(_file,"UTF-8").select("p").text();
			String mathcontent = Jsoup.parse(_file,"UTF-8").select("math").text();
			if ( content.isEmpty() ){
				return false;
			}
			
			String[] contentwords = content.replaceAll("\\p{P}|\\d", "").toLowerCase().split("\\s+");
			String[] mathwords = mathcontent.replaceAll("\\p{P}|\\d", "").toLowerCase().split("\\s+");
			
			List<String> contentlist = new ArrayList<String>();
			for(String word : contentwords){
				contentlist.add(word);
			}
			
			List<String> formulalist = new ArrayList<String>();
			for(String word : mathwords){
				formulalist.add(word);
			}
			
			contentlist.removeAll(formulalist);
			formulalist.clear();
			
			/* allow only characters between a and z */
			List<String> textfieldlist = new ArrayList<String>();
			short max = 255;
			for(String word : contentlist){			
		        word = word.replace("ä", "ae");
		        word = word.replace("ö", "oe");
		        word = word.replace("ü", "ue");
		        word = word.replace("ß", "ss");
				boolean undefChar = false;
				for(int i = 0; i < word.length(); i++){
					char c = word.charAt(i);
					
					if( c < 97 || c > 122 ){
						undefChar = true;
						break;
					}
				}
				
				if ( !undefChar){
					if ( max > 0 ){
						textfieldlist.add(word.replaceAll("\\R"," "));
						max--;
					}
					
					if ( word.length() >= 4){
						_content.add(word.replaceAll("\\R"," "));
					}
				}
			}
			
			StringBuilder text = new StringBuilder();
			for(String word : textfieldlist){
				text.append(word+" ");
			}
			
			_jsondata.put("text", text.toString());
			final String hash = hash(text.toString()); 
			_jsondata.put("hash", hash);
			_jsondata.put("file", _file.getName().replaceAll("\\s+", ""));
			
			return true;
			
		} catch (IOException ioe) {
			//ioe.printStackTrace();
			System.err.println("could not extract file content from : "+_file.getAbsolutePath() + " (invalid path)");
			return false;
		}
	}
	
	@Override
	public void removeStopwords() throws FileNotFoundException {
		Set<String> stopwords = new HashSet<String>();
		File file = new File(_stopwordpath);
		
		try {
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			
			boolean read = true;
			while( read ){
				String line = br.readLine();
				if ( line == null ){
					read = false;
					break;
				}
				
				if ( line.isEmpty() ){
					continue;
				}
				
				stopwords.add(line);

			}
			
			_content.removeAll(stopwords);
			br.close();
			fr.close();
		} catch ( IOException ioe){
			System.err.println("could not remove stopwords. '" + _stopwordpath + "' not found");
			ioe.printStackTrace();
		}
	}

	@Override
	public String hash(final String text){
		try {
			byte[] textbytes = text.getBytes("UTF-8");
			MessageDigest sha = MessageDigest.getInstance("SHA-512");
			sha.update(textbytes);
			byte[] shabytes = sha.digest();
			String hashwert = DatatypeConverter.printHexBinary(shabytes);
			return hashwert.toLowerCase();
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			e.printStackTrace();
			return String.valueOf(text.hashCode());
		}
	}
	
	@Override
	public Map<String, String> getJsonContent(){
		return _jsondata;
	}
	
	@Override
	public List<String> getDocumentContent(){
		return _content;
	}

	@Override
	public TextExtractor call() throws Exception {
		if ( !extractText() ){
			return this;
		}
		
		try {
			removeStopwords();
		} catch ( FileNotFoundException fnfe ){
			//fnfe.printStackTrace();
		}
		
		return this;
	}
}
