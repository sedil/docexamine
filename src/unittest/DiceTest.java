package unittest;

import org.junit.Assert;
import org.junit.Test;

import algorithm.DiceCoefficient;

public class DiceTest {
	
	private DiceCoefficient dice = new DiceCoefficient();

	@Test
	public void null_test() {
		double similiarity = dice.similarity(null, null);
		Assert.assertEquals(0.0, similiarity, 0);
	}
	
	@Test
	public void empty_test() {
		final String x = new String();
		final String y = new String();
		double similiarity = dice.similarity(x,y);
		Assert.assertEquals(0.0, similiarity, 0);
	}
	
	@Test
	public void equals_test() {
		final String x = new String("autobahn");
		final String y = new String("autobahn");
		double similiarity = dice.similarity(x,y);
		Assert.assertEquals(1.0, similiarity, 0);
	}
	
	@Test
	public void similiarity_test(){
		final String x = new String("autobahn");
		final String y = new String("auto");
		double similiarity = dice.similarity(x,y);
		Assert.assertEquals(0.6, similiarity, 0);
	}

}
