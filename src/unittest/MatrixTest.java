package unittest;

import org.junit.Assert;
import org.junit.Test;

import util.Matrix;

public class MatrixTest {
	
	final float[][] testmatrix = {{1,2,3},{4,5,6},{7,8,9}};
	final double[] testvector = {5,2,3};

	@Test
	public void createEmptyMatrix(){
		final float[][] zero = {};
		Matrix matrix = new Matrix(zero,0);
		Assert.assertEquals(0, matrix.getMatrix()[0][0],0);
	}
	
	@Test
	public void createMatrix(){
		Matrix matrix = new Matrix(testmatrix, testmatrix.length);
		Assert.assertEquals(1, matrix.getMatrix()[0][0],0);
		Assert.assertEquals(3, matrix.getMatrix()[0][2],0);
		Assert.assertEquals(8, matrix.getMatrix()[2][1],0);
	}
	
	@Test
	public void multiplyMatrix(){
		Matrix matrix = new Matrix(testmatrix, testmatrix.length);
		double[] vec = matrix.multiply(testvector);
		double[] result = {18,48,78};
		Assert.assertArrayEquals(result, vec, 0.001);
	}
	
	@Test
	public void transposeMatrix(){
		Matrix matrix = new Matrix(testmatrix, testmatrix.length);
		matrix.transpose();
		Assert.assertEquals(1, matrix.getMatrix()[0][0],0);
		Assert.assertEquals(4, matrix.getMatrix()[0][1],0);
		Assert.assertEquals(7, matrix.getMatrix()[0][2],0);
		Assert.assertEquals(2, matrix.getMatrix()[1][0],0);
		Assert.assertEquals(5, matrix.getMatrix()[1][1],0);
		Assert.assertEquals(8, matrix.getMatrix()[1][2],0);
		Assert.assertEquals(3, matrix.getMatrix()[2][0],0);
		Assert.assertEquals(6, matrix.getMatrix()[2][1],0);
		Assert.assertEquals(9, matrix.getMatrix()[2][2],0);
	}

}
