package unittest;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import algorithm.CosineSimilarity;

public class CosineTest {

	private CosineSimilarity cosine = new CosineSimilarity();

	@Test
	public void null_test() {
		double similiarity = cosine.similarity(null, null);
		Assert.assertEquals(0.0, similiarity, 0);
	}
	
	@Test
	public void empty_test() {
		final Map<String, Double> x = new HashMap<String, Double>();
		final Map<String, Double> y = new HashMap<String, Double>();
		double similiarity = cosine.similarity(x,y);
		Assert.assertEquals(0.0, similiarity, 0);
	}
	
	@Test
	public void equals_test() {
		Map<String, Double> x = new HashMap<String, Double>();
		Map<String, Double> y = new HashMap<String, Double>();
		
		x.put("auto", 0.2);
		x.put("baum", 0.7);
		x.put("bank", 0.5);
		
		y.putAll(x);
		
		double similiarity = cosine.similarity(x,y);
		Assert.assertEquals(1.0, similiarity, 0);
	}
	
	@Test
	public void similiarity_test(){
		Map<String, Double> x = new HashMap<String, Double>();
		Map<String, Double> y = new HashMap<String, Double>();
		
		x.put("auto", 0.2);
		x.put("baum", 0.7);
		x.put("bank", 0.5);
		
		y.put("auto", 0.5);
		y.put("haus", 0.4);
		y.put("park", 0.0);
		
		double similiarity = cosine.similarity(x,y);
		System.out.println(similiarity);
		Assert.assertEquals(0.1768319639224352, similiarity, 0.01);
	}

}
