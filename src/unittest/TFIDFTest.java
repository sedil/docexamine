package unittest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import algorithm.TFIDF;

public class TFIDFTest {
	
	TFIDF tfidf = new TFIDF();

	@Test
	public void computeTF_zero(){
		List<String> document = new ArrayList<String>();
		document.add("baum");
		document.add("apfel");
		document.add("mathematik");
		document.add("papier");
		document.add("cpu");
		
		double tf = tfidf.computeTF(document, "stadt");
		Assert.assertEquals(0.0, tf, 0.0);
	}
	
	@Test
	public void computeTF_hit(){
		List<String> document = new ArrayList<String>();
		document.add("baum");
		document.add("apfel");
		document.add("mathematik");
		document.add("papier");
		document.add("cpu");
		
		double tf = tfidf.computeTF(document, "apfel");
		Assert.assertEquals(0.2, tf, 0.0);
	}
	
	@Test
	public void computeIDF_zero(){
		List<String> documentOne = new ArrayList<String>();
		documentOne.add("baum");
		documentOne.add("apfel");
		documentOne.add("mathematik");
		
		List<String> documentTwo = new ArrayList<String>();
		documentTwo.add("papier");
		documentTwo.add("holz");
		documentTwo.add("berlin");
		
		List<String> documentThree = new ArrayList<String>();
		documentThree.add("zug");
		documentThree.add("stift");
		documentThree.add("internet");
		
		List<List<String>> docPool = new ArrayList<List<String>>();
		docPool.add(documentOne);
		docPool.add(documentTwo);
		docPool.add(documentThree);
		
		double idf = tfidf.computeIDF(docPool, "gewitter");
		Assert.assertEquals(0.0, idf, 0.0);
	}
	
	@Test
	public void computeIDF_one(){
		List<String> documentOne = new ArrayList<String>();
		documentOne.add("baum");
		documentOne.add("apfel");
		documentOne.add("mathematik");
		
		List<String> documentTwo = new ArrayList<String>();
		documentTwo.add("papier");
		documentTwo.add("holz");
		documentTwo.add("berlin");
		
		List<String> documentThree = new ArrayList<String>();
		documentThree.add("zug");
		documentThree.add("stift");
		documentThree.add("internet");
		
		List<List<String>> docPool = new ArrayList<List<String>>();
		docPool.add(documentOne);
		docPool.add(documentTwo);
		docPool.add(documentThree);
		
		double idf = tfidf.computeIDF(docPool, "papier");
		Assert.assertEquals(Math.log10(docPool.size()/1), idf, 0.0);
	}
	
	@Test
	public void computeIDF_many(){
		List<String> documentOne = new ArrayList<String>();
		documentOne.add("baum");
		documentOne.add("apfel");
		documentOne.add("mathematik");
		
		List<String> documentTwo = new ArrayList<String>();
		documentTwo.add("baum");
		documentTwo.add("holz");
		documentTwo.add("baum");
		
		List<String> documentThree = new ArrayList<String>();
		documentThree.add("zug");
		documentThree.add("stift");
		documentThree.add("eis");
		
		List<List<String>> docPool = new ArrayList<List<String>>();
		docPool.add(documentOne);
		docPool.add(documentTwo);
		docPool.add(documentThree);
		
		double idf = tfidf.computeIDF(docPool, "baum");
		System.out.println(idf);
		Assert.assertEquals(Math.log10(docPool.size()/2), idf, 0.05);
	}

}
