package util;

/**
 * A util class for a wordpair
 * @author sebastian
 *
 */
public class WordPair {
	
	private final String _left, _right;
	
	/**
	 * Constructor which accepts two parameters
	 * @param left the left element
	 * @param right the right element
	 */
	public WordPair(final String left, final String right){
		_left = left;
		_right = right;
	}
	
	@Override
	public boolean equals(final Object obj){
		if ( obj == null ){
			return false;
		}
		
		if ( obj == this ){
			return true;
		}
		
		if ( obj instanceof WordPair ){
			WordPair p = (WordPair) obj;
			
			String l = p.getLeftElement();
			String r = p.getRightElement();
			if ( _left.equals(l) && _right.equals(r)){
				return true;
			}
		}
		return false;
	}
	
	@Override
	public int hashCode(){
		return _left.hashCode() + _right.hashCode();
	}
	
	/**
	 * returns the left element
	 * @return left word
	 */
	public String getLeftElement(){
		return _left;
	}
	
	/**
	 * returns the right element
	 * @return right word
	 */
	public String getRightElement(){
		return _right;
	}

}
