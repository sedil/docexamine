package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * This class provides access to the REST API from the university Leipzig
 * @author sebastian
 *
 */
public class CorporaService {
	
	private String jsonString = null;
	private String corpus = null; //"deu_news_2012_1M", deu_wikipedia_2010_1M, eng_news_2013_1M 
	
	/**
	 * Constructor
	 */
	public CorporaService() { }
	
	/**
	 * provides all available corpora
	 * @return true, if one corpus was choosen, false otherwise
	 */
	public boolean getAvailableCorpora(){
		final String urlpattern = "http://api.corpora.uni-leipzig.de/ws/corpora/availableCorpora";
		
		try {
			boolean ok = connect(urlpattern);
			if ( !ok ){
				System.err.println("could not connect to : " + urlpattern);
				return false;
			}
		} catch ( IOException ioe ){
			ioe.printStackTrace();
			System.err.println("could not connect to : " + urlpattern);
		}
		
		JSONArray outer = new JSONArray(jsonString);
		
		List<String> corpuslist = new ArrayList<String>();
		for(int i = 0; i < outer.length(); i++){
			JSONObject inner = outer.getJSONObject(i);
			corpuslist.add(i +" " + inner.getString("corpusName"));
		}
		
		for(String l : corpuslist){
			System.out.println(l);
		}
		
		Scanner input = new Scanner(System.in);
		System.out.println("Choose one of the available corpora. Enter a number");
		int choose = input.nextInt();
		input.close();
		
		if ( choose >= 0 && choose < corpuslist.size() ){
			corpus = corpuslist.get(choose);
		} else {
			corpus = "deu_news_2012_1M";
		}
		return true;
	}
	
	/**
	 * returns a list of word cooccurrences to a given word
	 * @param word word to find cooccurrences
	 * @param limit maximal amount of cooccurrences (if available)
	 * @return list contains word cooccurrences
	 */
	public List<String> getCooccurrences(String word, int limit){
		final String urlpattern = "http://api.corpora.uni-leipzig.de/ws/cooccurrences/" + corpus + "/cooccurrences/" + word + "?limit=" + limit;

		try {
			boolean ok = connect(urlpattern);
			if ( !ok ){
				return new ArrayList<String>();
			}
		} catch ( IOException ioe ){
			ioe.printStackTrace();
			System.err.println("could not connect to : " + urlpattern);
		}
		
		JSONArray outer = new JSONArray(jsonString);
		
		List<String> cooc = new ArrayList<String>();
		for(int i = 0; i < outer.length(); i++){
			JSONObject elem = outer.getJSONObject(i).getJSONObject("w2");
			cooc.add(elem.getString("word"));
		}
		return cooc;
	}
	
	/**
	 * returns a list of related words to a given word
	 * @param word word to find related words
	 * @return list contains related words
	 */
	public List<String> getRelatedWords(String word){
		final String urlpattern = "http://api.corpora.uni-leipzig.de/ws/words/" + corpus + "/wordrelations/" + word;

		try {
			boolean ok = connect(urlpattern);
			if ( !ok ){
				return new ArrayList<String>();
			}
		} catch ( IOException ioe ){
			ioe.printStackTrace();
			System.err.println("could not connect to : " + urlpattern);
		}
		
		JSONArray outer = new JSONArray(jsonString);
		
		List<String> related = new ArrayList<String>();
		for(int i = 0; i < outer.length(); i++){
			JSONObject elem = outer.getJSONObject(i);
			related.add(elem.getString("word2"));
		}
		
		return related;
	}
	
	/**
	 * returns a list of related documents to a given word
	 * @param word word to find related documents
	 * @param offset offset
	 * @param limit maximal amount of related docs (if available)
	 * @return list contains infos about related documents
	 */
	public List<String> getRelatedDoc(String word, int offset, int limit){
		final String urlpattern = "http://api.corpora.uni-leipzig.de/ws/sentences/" + corpus + "/sentences/" + word + "/?offset=" + offset + "&limit=" + limit;
		
		try {
			boolean ok = connect(urlpattern);
			if ( !ok ){
				return new ArrayList<String>();
			}
		} catch ( IOException ioe ){
			ioe.printStackTrace();
			System.err.println("could not connect to : " + urlpattern);
		}
		
		JSONObject json = new JSONObject(jsonString);
		JSONArray outer = json.getJSONArray("sentences");
		JSONObject inner = outer.getJSONObject(0).getJSONObject("source");
		
		List<String> infos = new ArrayList<String>();
		infos.add(inner.getString("id"));
		infos.add(inner.getString("date"));
		infos.add(inner.getString("url"));
		return infos;
	}
	
	/**
	 * tries to connect to the web service of the university of Leipzig
	 * @param urlpattern specific url
	 * @return true, if connection was successfully, false otherwise
	 * @throws IOException exception
	 */
	private boolean connect(String urlpattern) throws IOException {
		URL url = new URL(urlpattern);
		HttpURLConnection connect = (HttpURLConnection) url.openConnection();
		connect.setRequestMethod("GET");
		int responseCode = connect.getResponseCode();
		
		if ( responseCode != 200 ){
			return false;
		}
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(connect.getInputStream()));
		String input;
		StringBuffer buffer = new StringBuffer();
		while((input = reader.readLine()) != null ) {
			buffer.append(input);
		}
		reader.close();
		jsonString = buffer.toString();
		return true;
	}

}
