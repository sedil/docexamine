package util;

import java.util.HashSet;
import java.util.Set;

/**
 * A helperclass to receive data for the pagerank and hits
 * algorithm
 * @author sebastian
 *
 */
public class TempData {
	
	private Set<String> _keywords;
	private TermVector _termvector;
	private final int IDX;
	
	/**
	 * Constructor
	 * @param index index/id of the document
	 */
	public TempData(int index){
		IDX = index;
		_keywords = new HashSet<String>();
		_termvector = new TermVector();
	}
	
	/**
	 * returns a set of keywords
	 * @return keywords
	 */
	public Set<String> getKeywordSet(){
		return _keywords;
	}
	
	/**
	 * returns a termvector which contains keys and their
	 * weights
	 * @return termvector
	 */
	public TermVector getTermVector(){
		return _termvector;
	}
	
	/**
	 * returns the document-id/index
	 * @return index
	 */
	public int getIndex(){
		return IDX;
	}

}
