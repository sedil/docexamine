package util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * this class provides a termvector. It contains a map with words
 * as keys and weights as values
 * @author sebastian
 *
 */
public class TermVector {
	
	private Map<String, Double> _termweights;
	
	/**
	 * Default Constructor which initialised a map
	 */
	public TermVector(){
		_termweights = new LinkedHashMap<String, Double>();
	}
	
	/**
	 * adds a word with the calculated weight to the map
	 * @param term a word as a string
	 * @param weight the weightvalue of the term
	 */
	public void addTerm(final String term, final double weight){
		if ( _termweights.containsKey(term) ){
			return;
		} else {	
			_termweights.put(term, weight);
		}
	}
	
	/**
	 * for Debugging : prints the map
	 */
	public void printMap(){
		for(Map.Entry<String, Double> entry : _termweights.entrySet()){
			System.out.println(entry.getKey()+ " "+entry.getValue());
		}
	}
	
	/**
	 * sorts a map by weight values
	 */
	public void sortMap(){
		List<Entry<String, Double>> list = new ArrayList<Entry<String, Double>>(_termweights.entrySet());
		
		Collections.sort(list, new Comparator<Entry<String, Double>>(){

			@Override
			public int compare(Entry<String, Double> x,
					Entry<String, Double> y) {
				return (y.getValue()).compareTo(x.getValue());
			}
		});
		
		Map<String, Double> sorted = new LinkedHashMap<String, Double>();
		for(Entry<String, Double> entry : list ){
			sorted.put(entry.getKey(), entry.getValue());
		}
		
		_termweights.clear();
		_termweights.putAll(sorted);
	}
	
	/**
	 * sorts a map and returns the result
	 * @param map unsorted map
	 * @return sorted map
	 */
	public static <K, V extends Comparable<? super V>> Map<K,V> sortByValue(Map<K,V> map){
		List<Entry<K, V>> temp = new ArrayList<Entry<K,V>>(map.entrySet());
		Collections.sort(temp, new Comparator<Entry<K,V>>(){
			
			@Override
			public int compare(Map.Entry<K,V> x, Map.Entry<K,V> y){
				return (y.getValue().compareTo(x.getValue()));
			}
		});
		
		Map<K,V> sorted = new LinkedHashMap<K,V>();
		for(Map.Entry<K, V> elem : temp){
			sorted.put(elem.getKey(), elem.getValue());
		}
		return sorted;
	}

	/**
	 * get the weightvector of the terms
	 * @return weightvector of all terms in the document
	 */
	public double[] getWeightVector(){
		Object[] temp = _termweights.values().toArray();
		double[] weightvector = new double[temp.length];
		for(int i = 0; i < temp.length; i++){
			weightvector[i] = (double) temp[i];
		}	
		return weightvector;
	}
	
	/**
	 * get the first n keywords from the termvector
	 * @param num amount of keywords
	 * @return a list containing the first n keywords
	 */
	public Set<String> getNKeywords(final int num){
		Set<String> keywords = new HashSet<String>();
		
		for(Map.Entry<String, Double> entry : _termweights.entrySet()){
			if ( keywords.size() < num ){
				keywords.add(entry.getKey());
			} else {
				break;
			}
		}
		return keywords;
	}
	
	/**
	 * returns the termvector which contains a word and his weight
	 * @return the termvector
	 */
	public Map<String, Double> getTermVector(){
		return _termweights;
	}

}
