package util;

/**
 * This class contains a matrix for the Pagerank and Hits algorithm
 * @author sebastian
 */
public class Matrix {
	
	private final int SIZE;
	private float[][] _matrix;
	
	// [zeile][spalte]
	
	/**
	 * Constructor to initilizes a matrix with a fixed size
	 * @param matrix <T> valued matrix
	 * @param size the matrix size
	 */
	public Matrix(final float[][] matrix, final int size){
		if ( size == 0){
			SIZE = 1;
			final float[][] dummy = {{0}};
			_matrix = dummy;
		} else {
			SIZE = size;
			_matrix = matrix;
		}
	}
	
	/**
	 * multiplies the matrix with a vector and returns the
	 * result as a vector
	 * @param vector a vector with fixed size
	 * @return result of this calculation
	 */
	public double[] multiply(final double[] vector){
		double[] result = new double[SIZE];

		for(int i = 0; i < SIZE; i++){
			double sum = 0.0;
			for(int j = 0; j < SIZE; j++){
				sum += (_matrix[i][j] * vector[j]);
			}
			result[i] = sum;
		}
		
		return result;
	}
	
	/**
	 * transposes the matrix
	 */
	public void transpose(){
		float[][] temp = new float[SIZE][SIZE];
		for(int i = 0; i < SIZE; i++){
			for(int j = 0; j < SIZE; j++){
				temp[j][i] = (Float) _matrix[i][j];
			}
		}
		_matrix = temp;
	}
	/**
	 * returns the matrix
	 * @return a double valued matrix
	 */
	public float[][] getMatrix(){
		return _matrix;
	}
	
	/**
	 * returns the amount of rows/columns of this
	 * matrix
	 * @return size of matrix
	 */
	public int getSize(){
		return SIZE;
	}
	
	/**
	 * For Debugging. Prints all elements of the matrix
	 */
	public void printMatrix(){
		for(int i = 0; i < SIZE; i++){
			System.out.print(_matrix[i][0]+" ");
			for(int j = 1; j < SIZE; j++){
				System.out.print(_matrix[i][j]+" ");
			}
			System.out.println();
		}
	}

}
