package util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This class represents a graph for computing the
 * PAGERANK and HITS Algorithm
 * @author sebastian
 *
 */
public class Graph {
	
	public Map<Node, Set<Node>> _graph;
	private int _id_counter;
	private final int NUM_KEYS;
	
	/**
	 * The Constructor creates and initialises a map.
	 * This Map is the graphrepresentation. The key is a node which
	 * contains a string, an id and a weight
	 * @param number_of_keywords number of keywords
	 */
	public Graph(final int number_of_keywords){
		NUM_KEYS = number_of_keywords;
		_graph = new HashMap<Node, Set<Node>>();
		_id_counter = 0;
	}
	
	/**
	 * insert a node in the graph
	 * @param word the word to be inserted
	 */
	public void insertNode(final String word){
		
		for(Map.Entry<Node, Set<Node>> elem : _graph.entrySet()){			
			Node test = elem.getKey();
			
			if ( test.getWord() == word ){
				return;
			}
		}
		_graph.put(new Node(word, _id_counter), new HashSet<Node>());
		_id_counter++;
	}
	
	/**
	 * inserts an 'edge' between two nodes
	 * @param start the left or start node
	 * @param end the right or end node
	 */
	public void insertEdge(final String start, final String end){
		
		for(Map.Entry<Node, Set<Node>> x : _graph.entrySet() ){
			Node s = x.getKey();
			
			if( s.getWord() == start ){			
				for(Map.Entry<Node, Set<Node>> y : _graph.entrySet() ){
					Node e = y.getKey();
					
					if ( e.getWord() == end ){
						_graph.get(s).add(e);
						break;
					}
				}
				break;
			}
		}
	}
	
	/**
	 * transformes the graph into an adjacency matrix.
	 * This works only if all nodes has an unique id
	 * @return the adjacency matrix of the graph
	 */
	public float[][] transformIntoAdjacencMatrix(){
		float[][] adj = new float[_graph.size()][_graph.size()];
		
		for(Map.Entry<Node, Set<Node>> entry : _graph.entrySet()){
			int i = entry.getKey().getID();
			Set<Node> edges = entry.getValue();

			for(Node n : edges){
				int j = n.getID();
				adj[i][j] = 1;
			}
		}	
		return adj;	
	}
	
	/**
	 * transformes the graph into an adjacency matrix.
	 * This works only if all nodes has an unique id
	 * @return the adjacency matrix of the graph
	 */
	public float[][] transformIntoWeightMatrix(){
		float[][] adj = new float[_graph.size()][_graph.size()];
		
		for(Map.Entry<Node, Set<Node>> entry : _graph.entrySet()){
			int i = entry.getKey().getID();
			Set<Node> edges = entry.getValue();

			for(Node n : edges){
				int j = n.getID();
				
				/* initial the node weight with 1/#words */
				adj[i][j] = (1/(float) _graph.size());
			}
		}	
		return adj;	
	}
	
	/**
	 * returns the keyword with the maximum weight of this map
	 * @param map map to extract the keywords
	 * @return keyword
	 */
	private String getKeyword(final Map<String, Double> map){
		double max = -1.0;
		String keyword = null;
		for(Map.Entry<String, Double> elem : map.entrySet() ){
			double t = elem.getValue();
			if( t > max ){
				max = t;
				keyword = elem.getKey();
			}
		}
		map.remove(keyword);
		return keyword;
	}
	
	/**
	 * sorts a map by weightvalue
	 * @param mapToSort map which contains words and weights
	 * @return list with keywords
	 */
	public List<String> sortByValue(final Map<String, Double> mapToSort){
		
		List<String> keywords = new ArrayList<String>();
		for(int i = 0; i < NUM_KEYS; i++){
			if ( mapToSort.size() > 0){
				String key = getKeyword(mapToSort);
				keywords.add(key);
			} else {
				break;
			}
		}
		
		return keywords;
	}
	
	/**
	 * returns all nodex
	 * @return nodex
	 */
	public Set<Node> getWords(){
		return _graph.keySet();
	}
	
	/**
	 * prints the graph as string
	 * @return graph as string
	 */
	public String getGraphAsString(){
		StringBuilder sb = new StringBuilder();
		for(Map.Entry<Node, Set<Node>> elem : _graph.entrySet()){
			sb.append(elem.getKey().getWord()+" ["+elem.getKey().getID()+"] -> ");
			for(Node end : elem.getValue()){
				sb.append(end.getWord()+"["+end.getID()+"] ");
			}
			sb.append("\n");
		}
		return sb.toString();
	}
	
	/**
	 * returns a word by node id
	 * @param id node id
	 * @return word
	 */
	public String getNodeWordByID(int id){
		for(Map.Entry<Node, Set<Node>> match : _graph.entrySet()){
			if ( match.getKey().getID() == id){
				return new String(match.getKey().getWord());
			}
		}
		return new String();
	}
}