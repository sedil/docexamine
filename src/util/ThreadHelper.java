package util;

/**
 * A helper class for the multithreading tasks
 * @author sebastian
 *
 * @param <T> a generic parameter
 */
public class ThreadHelper<T> {
	
	private final int IDX;
	private T _data;
	
	/**
	 * Constructor to hold a index and a datatype
	 * @param data datatype like map or lists
	 * @param idx index to identify
	 */
	public ThreadHelper(T data, int idx){
		_data = data;
		IDX = idx;
	}
	
	/**
	 * returns the data
	 * @return data
	 */
	public T getData(){
		return _data;
	}
	
	/**
	 * returns the index to find the right order
	 * after thread tasks
	 * @return index
	 */
	public int getIdx(){
		return IDX;
	}

}
