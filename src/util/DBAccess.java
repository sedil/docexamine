package util;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

/**
 * This class provides an access to the MongoDB
 * @author sebastian
 *
 */
public class DBAccess {
	
	private final String HOST;
	private final int PORT;
	private String _user;
	private String _password;
	private String _database;
	private String _collection;
	
	private MongoClient _mongo;
	private MongoDatabase _mongoDB;
	private MongoCollection<Document> _mongoCol;
	
	/**
	 * Constructor
	 * @param host the hostname for the connection
	 * @param port the port for the MongoDB (27017)
	 */
	public DBAccess(final String host, final int port){
		HOST = host;
		PORT = port;
	}
	
	/**
	 * initializes the userdata
	 * @param user username
	 * @param password password of this user
	 */
	public void setUserdata(final String user, final String password ){
		_user = user;
		_password = password;
	}
	
	/**
	 * sets the name of the database and the collection for documents
	 * @param database databasename
	 * @param collection collectionname
	 */
	public void setMongoDBData(final String database, final String collection){
		_database = database;
		_collection = collection;
	}
	
	/**
	 * connects to the MongoDB
	 */
	public void connectToDatabase(){
		_mongo = new MongoClient(HOST, PORT);
		MongoCredential credential;
		credential = MongoCredential.createCredential(_user, _database, _password.toCharArray());
		System.out.println("Connected to the database");
		System.out.println("Credential : " + credential);
	}
	
	/**
	 * inserts one document into the database/collection
	 * @param doc
	 * @return true, if connection was successfully
	 */
	public boolean insertDocument(final Document doc){
	
		try {
			_mongoDB = _mongo.getDatabase(_database);
			for(String c : _mongoDB.listCollectionNames() ){
				
				/* find existing collections */
				if ( c.equals(_collection) ){	
					_mongoCol = _mongoDB.getCollection(_collection);
					_mongoCol.insertOne(doc);
					System.out.println("Document inserted in collection");
					return true;
				}
			}
			
			/* collection does not exist, create this one and insert document */
			_mongoDB.createCollection(_collection);
			_mongoCol = _mongoDB.getCollection(_collection);
			_mongoCol.insertOne(doc);
			System.out.println("Collection created successfully");
			System.out.println("Document inserted in collection");
			return true;
		} catch ( Exception e){
			System.out.println("Could not insert JSON into database. Saving in results folder");
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * deletes a whole collection
	 */
	public void deleteCollection(){
		_mongoCol.drop();
	}
	
	/**
	 * closes the connection the the database
	 */
	public void close(){
		_mongo.close();
	}
}
